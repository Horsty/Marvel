import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MoviesComponent } from '@app/features/driveIn/components/movie/movies.component';
import { ProfilComponent } from '@app/features/driveIn/components/movie/profil/profil.component';

const routes = [
    {
        path: '', component: MoviesComponent
    },
    {
        path: ':id',
        component: ProfilComponent
    }];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MoviesRoutingModule { }
