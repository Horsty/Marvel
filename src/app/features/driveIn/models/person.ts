import { IMovie } from '@app/features/driveIn/models/movie';

export interface IPerson {
    id: number;
    movies: [IMovie];
    name: string;
    profilePath: string;
    externId: number;
}
