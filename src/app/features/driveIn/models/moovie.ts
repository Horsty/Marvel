import { IPerson } from '@app/features/driveIn/models/person';

export interface IMoovie {
    id: number;
    title: string;
    releaseDate: Date;
    synopsis: string;
    person: [IPerson];
    externId: number;
    posterPath: string;
}
