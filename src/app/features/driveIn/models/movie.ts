import { IPerson } from '@app/features/driveIn/models/person';

export interface IMovie {
    id: number;
    title: string;
    releaseDate: Date;
    synopsis: string;
    persons: [IPerson];
    externId: number;
    posterPath: string;
}
