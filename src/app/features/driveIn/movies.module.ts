import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MoviesComponent } from '@app/features/driveIn/components/movie/movies.component';
import { MoviesRoutingModule } from '@app/features/driveIn/movies-routing.module';
import { CardComponent } from '@app/features/driveIn/components/movie/card/card.component';
import { ProfilComponent } from '@app/features/driveIn/components/movie/profil/profil.component';

@NgModule({
    imports: [
        CommonModule, MoviesRoutingModule
    ],
    declarations: [MoviesComponent, CardComponent, ProfilComponent]
})
export class MoviesModule { }
