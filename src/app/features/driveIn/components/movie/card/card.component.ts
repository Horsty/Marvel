import { Component, OnInit, Input } from '@angular/core';
import { IMovie } from '@app/features/driveIn/models/movie';

@Component({
    selector: 'app-card',
    templateUrl: './card.component.html',
    styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
    @Input()
    movie: IMovie;
    constructor() { }

    ngOnInit() {
    }

}
