import { Component, OnInit, OnDestroy } from '@angular/core';
import { MoviesService } from '@app/features/driveIn/movies.service';

@Component({
    selector: 'app-movies',
    templateUrl: './movies.component.html',
    styleUrls: ['./movies.component.scss']
})
export class MoviesComponent implements OnInit, OnDestroy {
    movies;
    movieObs;
    constructor(private moviesDao: MoviesService) { }

    ngOnInit() {
        this.movieObs = this.moviesDao.getMovies().subscribe(
            res => { this.movies = res; });
    }

    ngOnDestroy() {
        this.movieObs.unsubscribe();
    }
}
