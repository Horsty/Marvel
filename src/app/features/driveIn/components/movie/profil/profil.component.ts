import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IMovie } from '@app/features/driveIn/models/movie';
import { MoviesService } from '@app/features/driveIn/movies.service';
import { IPerson } from '@app/features/driveIn/models/person';
import { Location } from '@angular/common';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.scss']
})
export class ProfilComponent implements OnInit {
  id: number;
  private sub: any;
  movie: IMovie;
  person: IPerson;
  constructor(private route: ActivatedRoute, private driveIn: MoviesService, private location: Location) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id']; // (+) converts string 'id' to a number
      // In a real app: dispatch action to load the details here.
    });
    this.driveIn.getMovie(this.id).subscribe(res => {
      this.movie = res; console.log(res);
    });
  }
  goBack() {
    // window.history.back();
    this.location.back();
  }

}
