import { Injectable } from '@angular/core';
import { MoviesModule } from '@app/features/driveIn/movies.module';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IMoovie } from './models/moovie';
import { IMovie } from './models/movie';
import { IPerson } from './models/person';

@Injectable({
  providedIn: MoviesModule
})
export class MoviesService {
  uri = 'https://api.horsty.fr/';
  constructor(private http: HttpClient) { }

  getMovies(): Observable<IMoovie[]> {
    return this.http.get<IMoovie[]>(this.uri + 'movies/');
  }

  getPeople(): Observable<IPerson[]> {
    return this.http.get<IPerson[]>(this.uri + 'persons/');
  }

  getMovie(id: number): Observable<IMovie> {
    return this.http.get<IMovie>(this.uri + 'movies/' + id);
  }

  getPerson(id: number): Observable<IPerson> {
    return this.http.get<IPerson>(this.uri + 'persons/' + id);

  }
}
