import { NgModule } from '@angular/core';
import { TaskComponent } from '@app/features/tasks/task.component';
import { TaskRoutingModule } from '@app/features/tasks/task-routing.module';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [TaskRoutingModule, CommonModule, FormsModule],
  declarations: [TaskComponent]
})
export class TaskModule { }
