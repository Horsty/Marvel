import { TestBed, inject } from '@angular/core/testing';

import { TasksDaoService } from './tasks-dao.service';

describe('TasksDaoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TasksDaoService]
    });
  });

  it('should be created', inject([TasksDaoService], (service: TasksDaoService) => {
    expect(service).toBeTruthy();
  }));
});
