import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TaskComponent } from '@app/features/tasks/task.component';

const routes = [{
  path: '',
  component: TaskComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TaskRoutingModule { }
