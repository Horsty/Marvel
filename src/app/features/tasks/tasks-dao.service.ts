import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestoreDocument, AngularFirestore } from '@angular/fire/firestore';
import { ITask } from '@app/features/tasks/task';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class TasksDaoService {
  private endPoint = 'tasks';
  tasks: AngularFirestoreCollection<ITask>;
  private taskDoc: AngularFirestoreDocument<ITask>;
  constructor(private db: AngularFirestore) {
    // Get the tasks collection
    this.tasks = db.collection<ITask>(this.endPoint);
  }
  search() {
    return this.tasks
      .snapshotChanges().pipe(
        map(actions => actions.map(a => {
          const data = a.payload.doc.data() as ITask;
          const id = a.payload.doc.id;
          return { id, ...data };
        }))
      );
  }

  addTask(task) {
    // Add the new task to the collection
    this.tasks.add(task);
  }

  updateTask(id, update) {
    // Get the task document
    this.taskDoc = this.db.doc<ITask>(`${this.endPoint}/${id}`);
    this.taskDoc.update(update);
  }

  deleteTask(id) {
    // Get the task document
    this.taskDoc = this.db.doc<ITask>(`${this.endPoint}/${id}`);
    this.taskDoc.delete();
  }


}
