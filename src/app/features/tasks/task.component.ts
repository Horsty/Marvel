import { Component, OnInit, OnDestroy } from '@angular/core';
import { TasksDaoService } from '@app/features/tasks/tasks-dao.service';
import { LoaderService } from '../loader/loader.service';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss']
})
export class TaskComponent implements OnInit, OnDestroy {
  tasks: any;
  myTask: string;
  editMode = false;
  loading = false;
  taskToEdit: any = {};
  taskObs;
  constructor(private tasksService: TasksDaoService, private loaderService: LoaderService) {
  }

  ngOnInit() {
    this.loaderService.isLoading.next(true);
    this.taskObs = this.tasksService.search().subscribe(
      tasks => {
        this.tasks = tasks;
        this.loaderService.isLoading.next(false);
      }
    );
  }

  ngOnDestroy() {
    this.taskObs.unsubscribe();
  }

  saveTask() {
    if (this.myTask !== null) {
      // Get the input value
      const task = {
        description: this.myTask
      };
      if (!this.editMode) {
        this.tasksService.addTask(task);
      } else {
        // Get the task id
        const taskId = this.taskToEdit.id;
        // update the task
        this.tasksService.updateTask(taskId, task);
      }
      // set edit mode to false and clear form
      this.editMode = false;
      this.myTask = '';
    }
  } // saveTask

  edit(task) {
    console.log(task);
    // Set taskToEdit and editMode
    this.taskToEdit = task;
    this.editMode = true;
    // Set form value
    this.myTask = task.description;
  } // edit

  deleteTask(task) {
    // Get the task id
    const taskId = task.id;
    // delete the task
    this.tasksService.deleteTask(taskId);
  } // deleteTask

}
