import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileRoutingModule } from './profile-routing.module';
import { FormsModule } from '@angular/forms';
import { ProfileComponent } from './components/profile.component';

@NgModule({
  imports: [
    ProfileRoutingModule,
    CommonModule,
    FormsModule
  ],
  declarations: [ProfileComponent]
})
export class ProfileModule { }
