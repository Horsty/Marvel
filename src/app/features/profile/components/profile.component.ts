import { Component, OnInit } from '@angular/core';
import { LoginService } from '@app/login/login.service';
import { IUser } from '@app/login/models/user';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  user: IUser;
  constructor(public auth: LoginService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.user = this.route.snapshot.data.currentUser;
  }

}
