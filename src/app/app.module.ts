import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';


/*
* Module firebase
AngularFireAuthModule
AngularFireDatabaseModule
AngularFireFunctionsModule
AngularFirestoreModule
AngularFireStorageModule
AngularFireMessagingModule
*/
import { AngularFireModule } from '@angular/fire';

import { AppComponent } from './app.component';
import { UiModule } from './ui/ui.module';
import { AppRoutingModule } from './app-routing.module';
import { ComponentsModule } from './components/components.module';
import { environment } from '@env/environment';
import { CoreModule } from '@app/core';
import { LoginRouteGuard } from './login-route.guard';
// import { CachingInterceptor } from './core/interceptors/caching-interceptor';
// import { RequestCache } from './core/interceptors/request-cache.service';
import { LoginResolver } from './login/login-resolver';
import { SharedModule } from './shared';
import { LoaderService } from './features/loader/loader.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { LoaderInterceptor } from './features/loader/loader-interceptor';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CustomMaterialModule } from './material/custom-material.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    CoreModule,
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase, 'ng-marvel-project-universe'),
    AppRoutingModule,
    UiModule,
    FormsModule,
    ComponentsModule,
    SharedModule,
    BrowserAnimationsModule,
    CustomMaterialModule
  ],
  exports: [],
  providers: [
    LoginRouteGuard,
    LoginResolver,
    LoaderService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoaderInterceptor,
      multi: true
    },
    // RequestCache,
    // { provide: HTTP_INTERCEPTORS, useClass: CachingInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
