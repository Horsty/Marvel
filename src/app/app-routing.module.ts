import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { PageHomeComponent } from '@app/components/page-home/page-home.component';
import { LoginRouteGuard } from './login-route.guard';
import { LoginResolver } from './login/login-resolver';

const routes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        component: PageHomeComponent,
        resolve: { currentUser: LoginResolver },
    },
    {
        path: '',
        canActivate: [LoginRouteGuard],
        resolve: { currentUser: LoginResolver },
        children: [
            {
                path: '',
                redirectTo: 'profile',
                pathMatch: 'full',
            },
            {
                path: 'profile',
                loadChildren: 'app/features/profile/profile.module#ProfileModule'
            },
            {
                path: 'tasks',
                loadChildren: 'app/features/tasks/task.module#TaskModule'
            },
            {
                path: 'movies',
                loadChildren: 'app/features/driveIn/movies.module#MoviesModule'
            }
        ]
    },
    // {
    //     path: '403',
    //     pathMatch: 'full',
    //     component: ForbiddenComponent
    // },
    {
        path: '**',
        pathMatch: 'full',
        component: PageNotFoundComponent
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
