import { NgModule } from '@angular/core';
import {
    MatProgressBarModule
} from '@angular/material';

@NgModule({
  imports: [MatProgressBarModule],
  exports: [MatProgressBarModule],
})
export class CustomMaterialModule { }
