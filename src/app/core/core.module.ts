/* 3rd party libraries */
import { NgModule, Optional, SkipSelf } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { CommonModule } from '@angular/common';
import { LoginService } from '@app/login/login.service';
import { AngularFireAuth } from '@angular/fire/auth';
/* our own custom services  */

@NgModule({
    imports: [
        /* 3rd party libraries */
        HttpClientModule,
        AngularFirestoreModule,
        CommonModule
    ],
    declarations: [],
    exports: [
        AngularFirestoreModule,
    ],
    providers: [
        /* our own custom services  */
        LoginService,
        AngularFireAuth
    ]
})
export class CoreModule {
    /* make sure CoreModule is imported only by one NgModule the AppModule */
    constructor(
        @Optional() @SkipSelf() parentModule: CoreModule
    ) {
        if (parentModule) {
            throw new Error('CoreModule is already loaded. Import only in AppModule');
        }
    }
}
