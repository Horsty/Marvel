import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

import { Observable } from 'rxjs';
import { tap, map, take } from 'rxjs/operators';
import { LoginService } from './login/login.service';

@Injectable()
export class LoginRouteGuard implements CanActivate {
    constructor(private auth: LoginService, private router: Router) { }


    canActivate(): Observable<boolean> {
        return this.auth.currentUser.pipe(
            take(1),
            map(user => !!user),
            tap(loggedIn => {
                if (!loggedIn) {
                    console.log('access denied');
                    this.router.navigate(['/']);
                }
            })
        );
    }
}
