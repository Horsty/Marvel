/* 3rd party libraries */
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from '../login/components/login.component';
import { CommonModule } from '@angular/common';
import { LoaderComponent } from '@app/features/loader/loader.component';


/* our own custom components */

@NgModule({
    imports: [
        /* angular stuff */
        FormsModule,
        ReactiveFormsModule,
        CommonModule
        /* 3rd party components */
    ],
    declarations: [
        LoginComponent,
        LoaderComponent
    ],
    exports: [
        /* angular stuff */
        FormsModule,

        /* 3rd party components */

        /* our own custom components */
        LoginComponent,
        LoaderComponent
    ]
})
export class SharedModule { }
