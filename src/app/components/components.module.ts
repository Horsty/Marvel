import { NgModule } from '@angular/core';
import { PageNotFoundComponent } from '../components/page-not-found/page-not-found.component';
import { PageHomeComponent } from './page-home/page-home.component';
import { SharedModule } from '../shared';
import { CoreModule } from '@app/core';

@NgModule({
    imports: [
        SharedModule,
        CoreModule
    ],
    declarations: [
        PageNotFoundComponent,
        PageHomeComponent
    ],
    exports: [
        PageNotFoundComponent,
        PageHomeComponent
    ]
})
export class ComponentsModule { }
