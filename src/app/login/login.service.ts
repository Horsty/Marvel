import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { auth, User } from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';

import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { environment } from '@env/environment';
import { IUser } from './models/user';

@Injectable({ providedIn: 'root' })
export class LoginService {
    private user: Observable<IUser>;
    email: string;
    emailSent: boolean;
    errorMessage: string;
    constructor(
        private afAuth: AngularFireAuth,
        private afs: AngularFirestore,
        private router: Router
    ) {
        //// Get auth data, then get firestore user document || null
        this.user = this.afAuth.authState.pipe(
            switchMap(user => {
                if (user) {
                    return this.afs.doc<IUser>(`users/${user.uid}`).valueChanges();
                } else {
                    return of(null);
                }
            })
        );
    }

    // Returns true if user is logged in
    get authenticated(): boolean {
        return this.user !== null;
    }

    // Returns current user
    get currentUser(): Observable<IUser> {
        return this.authenticated ? this.user : null;
    }

    googleLogin() {
        const provider = this.getProviderForMethod('google.com');
        return this.oAuthLogin(provider);
    }

    facebookLogin() {
        const provider = this.getProviderForMethod('facebook.com');
        return this.oAuthLogin(provider);
    }

    private oAuthLogin(provider) {
        return this.afAuth.auth.signInWithPopup(provider)
            .catch(this.handleError)
            .then((credential) => this.updateUserData(credential.user));
    }

    private handleError(error) {
        if (error.code === 'auth/account-exists-with-different-credential') {
            const pendingCred = error.credential;
            const email = error.email;
            // Get sign-in methods for this email.
            this.afAuth.auth.fetchSignInMethodsForEmail(email).then((methods) => {
                // If the user has several sign-in methods,
                // the first method in the list will be the "recommended" method to use.
                if (methods[0] === 'password') {
                    // @TODO Ask the user his password.
                    // handle this asynchronously.
                    const password = this.promptUserForPassword();
                    this.afAuth.auth.signInWithEmailAndPassword(email, password).then((result) => {
                        result.user.linkWithCredential(pendingCred);
                    }).then(() => {
                        // Facebook account successfully linked to the existing Firebase user.
                        // user storage, redirect, etc...
                    });
                }
                const provider = this.getProviderForMethod(methods[0]);
                // @TODO Informer l'utilisateur qu'une autre connexion existe
                // Si il accepte de linker les différents compte lancer la méthode signin
                this.afAuth.auth.signInWithPopup(provider).then((result) => {
                    result.user.linkAndRetrieveDataWithCredential(pendingCred).then((usercred) => {
                        // Facebook account successfully linked to the existing Firebase user.
                        // user storage, redirect, etc...
                    });
                });
            });
        }
        return error.credential;
    }

    /**
    * Retourne le provider associé à la méthode de connexion passé en paramétre
    * @param method
    */
    getProviderForMethod(method: string) {
        switch (method) {
            case 'google.com':
                return new auth.GoogleAuthProvider();
            case 'facebook.com':
                return new auth.FacebookAuthProvider();
        }
    }

    // TODO: implement promptUserForPassword.
    /**
     * Demande à l'utilisateur de saisir son mot de passe et le retourne
     */
    promptUserForPassword() {
        return 'password';
    }

    /**
     * Sets user data to firestore on login
     * @param user User
     */
    private updateUserData(user: User) {
        const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${user.uid}`);
        const data: IUser = {
            uid: user.uid,
            email: user.email,
            displayName: user.displayName,
            photoURL: user.photoURL
        };
        return userRef.set(data, { merge: true });
    }

    /**
     * Logout the current user and redirect to home
     */
    signOut(): void {
        this.afAuth.auth.signOut().then(() => {
            this.router.navigate(['/']);
        });
    }

    /**
     * Send a link with tokken to the mail address
     * @param email string
     */
    sendEmailLink(email: string): void {
        const actionCodeSettings = {
            // Your redirect URL
            url: environment.production ? 'https://horsty.fr/' : 'http://localhost:4200/',
            handleCodeInApp: true
        };
        this.afAuth.auth.sendSignInLinkToEmail(email, actionCodeSettings).then(() => {
            window.localStorage.setItem('emailForSignIn', email);
            this.emailSent = true;
        }).catch((err) => {
            this.errorMessage = err.message;
        });
    }

    /**
     * check validity mail / tokken
     * @param url string
     */
    confirmSignIn(url): void {
        if (this.afAuth.auth.isSignInWithEmailLink(url)) {
            let email = window.localStorage.getItem('emailForSignIn');
            // If missing email, prompt user for it
            if (!email) {
                email = window.prompt('Please provide your email for confirmation');
            }
            this.afAuth.auth.signInWithEmailLink(email, url).then(() => {
                window.localStorage.removeItem('emailForSignIn');
            }).catch((err) => {
                this.errorMessage = err.message;
            });
        }
    }
}

