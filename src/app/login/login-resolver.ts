import { Injectable } from '@angular/core';

import { Resolve } from '@angular/router';
import { LoginService } from './login.service';
import { of, Observable } from 'rxjs';
import { IUser } from './models/user';
import { map, take, first } from 'rxjs/operators';

@Injectable()
export class LoginResolver implements Resolve<Observable<IUser>> {
    constructor(private auth: LoginService) { }

    resolve() {
        return this.auth.currentUser.pipe(first());
    }
}
