import { Component, OnInit } from '@angular/core';
import { LoginService } from '../login.service';
import { Router, ActivatedRoute } from '@angular/router';
import { IUser } from '../models/user';
import { LoaderService } from '@app/features/loader/loader.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  email = '';
  user: IUser;
  constructor(public auth: LoginService, private route: ActivatedRoute, private router: Router, private loaderService: LoaderService) { }

  ngOnInit() {
    this.loaderService.isLoading.next(true);
    // to use resolver
    this.user = this.route.snapshot.data.currentUser;
    // to follow any changes
    this.auth.currentUser
    .subscribe(data => {
      this.user = data;
      this.loaderService.isLoading.next(false);
    });
    const url = this.router.url;
    if (url.includes('signIn')) {
      this.auth.confirmSignIn(url);
    }
  }

}
